﻿namespace WebAPI
{
    public class Service
    {

        public string Obchodné_meno { get; set; }

        public string Prevádzka { get; set; }

        public string Činnosť { get; set; }

        public string Ulica { get; set; }

        public string Sup_c { get; set; }

        public string Or_c { get; set; }

        public string Obec { get; set; }

        public string Adresa_prevádzky { get; set; }

        public string IČO { get; set; }

        public string Dátum_začatia_činnosti { get; set; }

        public string Prevádzková_doba_pondelok { get; set; }

        public string Prevádzková_doba_utorok { get; set; }

        public string Prevádzková_doba_streda { get; set; }

        public string Prevádzková_doba_štvrtok { get; set; }

        public string Prevádzková_doba_piatok { get; set; }

        public string Prevádzková_doba_sobota { get; set; }

        public string Prevádzková_doba_nedeľa { get; set; }
    }
}