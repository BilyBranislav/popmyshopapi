﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;

namespace WebAPI.Controllers
{
    public class ProductsController : ApiController
    {
        Product[] products = new Product[] {
            new Product {ID = 1, Name = "Macbook", LastName = "Pro" },
            new Product {ID = 2, Name = "Lenovo", LastName = "Thinkpad" },
            new Product {ID = 3, Name = "HP", LastName = "Whatever" },
            new Product {ID = 4, Name = "Acer", LastName = "Republic of gamers"},
            new Product {ID = 5, Name = "Asus", LastName = "Thinkapd?"},
            new Product {ID = 6, Name = "Dell" , LastName = "Y8"}
            };

        public IEnumerable<Product> GetAllProducts()
        {
            return products;
        }

        public Product GetProductById(int id)
        {
            var product = products.FirstOrDefault((p) => p.ID == id);
            if(product == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            return product;
        }

        public IEnumerable<Product> GetProductByName(string name)
        {
            return products.Where((p) => string.Equals(p.Name, name, StringComparison.OrdinalIgnoreCase));
        }
    }
}
