﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using WebAPI.Loaders;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ServicesController : ControllerBase
    {
        // GET api/services
        [HttpGet]
        public IEnumerable<Service> Get()
        {
            string url = "file:///Users/branislavbily/Projects/WebAPISolution/WebAPI/Controllers/Zoznam_Zoznam_obchodny%CC%81ch_preva%CC%81dzok_v_meste_Pres%CC%8Cov.json";
            List<Service> result = new JSONLoader().DownloadSerializedJsonData<Service>(url);
            return result;

        }

        // GET api/services/id
        [HttpGet]
        [Route("search")]
        public IEnumerable<Service> ReturnServices([FromQuery] string serviceName)
        {
            var url ="https://egov.presov.sk/Default.aspx?NavigationState=163:0::plac580:_144026_5_8";
            using (WebClient wc = new WebClient())
            {
                var json = wc.DownloadString(url);
                var services = JsonConvert.DeserializeObject<List<Service>>(json);
                var servicesByName = services.Where(s => s.Obchodné_meno.Contains(serviceName));
                return servicesByName;
            }
        }

        [HttpGet]
        [Route("search/{serviceName}")]
        public IEnumerable<Service> ReturnServicesWithoutQuery(string serviceName)
        {
            var url = "https://egov.presov.sk/Default.aspx?NavigationState=163:0::plac580:_144026_5_8";
            using (WebClient wc = new WebClient())
            {
                var json = wc.DownloadString(url);
                var services = JsonConvert.DeserializeObject<List<Service>>(json);
                var servicesByName = services.Where(s => s.Obchodné_meno.Contains(serviceName));
                return servicesByName;
            }
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }


}

